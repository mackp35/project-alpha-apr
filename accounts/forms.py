from django.db import models
from django.forms import forms


class LoginPage(forms):
    username = (forms.CharField(max_length=150),)
    password = (
        forms.CharField(
            max_length=150,
            widget=forms.PasswordInput,
        ),
    )


def user_login(request):
    if request.method == "POST":
        form - LoginPage(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            return redirect("list_projects")
